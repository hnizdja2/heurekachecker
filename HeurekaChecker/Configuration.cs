﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeurekaChecker
{
    public class Configuration
    {
        public Configuration() { }

        public List<HeurekaItem> items { get; set; }
    }
}
