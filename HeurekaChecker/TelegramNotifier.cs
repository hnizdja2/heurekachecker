﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace HeurekaChecker
{
    public static class TelegramNotifier
    {
        public static void SendMessageFirstOccurence(HeurekaProduct product)
        {
            TelegramBotClient bot = new TelegramBotClient("550022561:AAGxMw9yQE5BeCwBOa217A5ohEtBTA2OQTE");
            String message = "First occurence of product #" + HashtagableName(product.Name) + ".\r\n" + 
                "Price: " + product.Price + " CZK. <a href=\"" + product.Hypertext + "\">[details]</a>" + Environment.NewLine;

            var b = bot.SendTextMessageAsync("@SmartHeurekaChecker", message, Telegram.Bot.Types.Enums.ParseMode.Html);
        }

        public static void SendMessagePriceChanging(HeurekaProduct product, double previousPrice)
        {
            TelegramBotClient bot = new TelegramBotClient("550022561:AAGxMw9yQE5BeCwBOa217A5ohEtBTA2OQTE");
            String message = "Product #" + HashtagableName(product.Name) + " has a new price.\r\n" + 
                "Previous price: " + previousPrice + " CZK\r\n" + 
                "New price: " + product.Price + " CZK.\r\n" + 
                "<a href=\"" + product.Hypertext + "\">[details]</a>" + Environment.NewLine;

            var b = bot.SendTextMessageAsync("@SmartHeurekaChecker", message, Telegram.Bot.Types.Enums.ParseMode.Html);
        }

        private static string HashtagableName(string name)
        {
            return name.Replace("-", "").Replace(" ", "");
        }
    }
}
