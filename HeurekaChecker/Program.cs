﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HeurekaChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Click start for checking heureka prices");
            Console.ReadLine();

            while (true)
            {
                HeurekaChecker.CheckPrices();
                Thread.Sleep(60000);
            }
        }
    }
}
