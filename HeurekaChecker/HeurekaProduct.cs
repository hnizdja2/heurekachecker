﻿namespace HeurekaChecker
{
    public class HeurekaProduct
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string Hypertext { get; set; }

        public HeurekaProduct(string name, double price, string hypertext)
        {
            Name = name;
            Price = price;
            Hypertext = hypertext;
        }
    }
}