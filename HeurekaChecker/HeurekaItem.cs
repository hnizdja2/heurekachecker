﻿namespace HeurekaChecker
{
    public class HeurekaItem
    {
        public HeurekaItem() { }
        public string CategoryURL { get; set; }
        public string Product { get; set; }
    }
}