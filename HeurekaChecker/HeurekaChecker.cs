﻿using Fizzler.Systems.HtmlAgilityPack;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace HeurekaChecker
{
    public static class HeurekaChecker
    {
        public static string configurationFile = "configuration.json";
        public static string heurekaFile = "heureka_backup.json";
        public static void CheckPrices()
        {
            Configuration config = Serializer.ReadFromFile<Configuration>(configurationFile);
            List<HeurekaProduct> heurekaProductList = new List<HeurekaProduct>();

            foreach (HeurekaItem item in config.items)
            {
                heurekaProductList.Add(ParseHeurekaOutput(item));
            }

            checkPriceUpdates(heurekaProductList);


            Serializer.SaveToFile<List<HeurekaProduct>>(heurekaFile, heurekaProductList);
        }

        private static void checkPriceUpdates(List<HeurekaProduct> heurekaProductList)
        {
            List<HeurekaProduct> savedProductList = Serializer.ReadFromFile<List<HeurekaProduct>>(heurekaFile);

            if (savedProductList == null)
            {
                savedProductList = new List<HeurekaProduct>();
            }

            foreach(HeurekaProduct product in heurekaProductList)
            {

                //first occurence - notify about price
                if(!savedProductList.Exists(x => x.Name == product.Name))
                {
                    savedProductList.Add(product);
                    //TODO: Notify about price
                    TelegramNotifier.SendMessageFirstOccurence(product);

                }else if(savedProductList.Exists(x => x.Name == product.Name && x.Price != product.Price))
                {
                    double previousPrice = savedProductList.SingleOrDefault(x => x.Name == product.Name).Price;
                    savedProductList.RemoveAll(x => x.Name == product.Name);
                    savedProductList.Add(product);

                    //TODO: Notify about changing price
                    TelegramNotifier.SendMessagePriceChanging(product, previousPrice);
                }
            }

            Serializer.SaveToFile<List<HeurekaProduct>>(heurekaFile, savedProductList);
           
        }

        private static string CallHeureka(HeurekaItem item)
        {
            String inputCommand = item.CategoryURL + item.Product;

            WebClient wc = new WebClient();
            wc.Encoding = UTF8Encoding.UTF8;

            String result = "";

            try
            {
                result = wc.DownloadString(inputCommand);
            }
            catch (WebException e)
            {
                //repeat call
                return CallHeureka(item);
            }

            return result;
        }

        static HeurekaProduct ParseHeurekaOutput(HeurekaItem item)
        {

            String htmlContent = CallHeureka(item);


           

            var html = new HtmlDocument();
            html.LoadHtml(htmlContent);

            var document = html.DocumentNode;
            var totalPrice = Convert.ToDouble(document.QuerySelector("span[itemprop='lowPrice']").InnerText);
            var productName = document.QuerySelector("h1[itemprop='name']").InnerText;

            return new HeurekaProduct(productName, totalPrice, item.CategoryURL + item.Product);
        }

    }
}
